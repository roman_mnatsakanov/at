package com.andersenlab;

public class factorial {
    public static void countFact(int fact) {
        int fact_res = 1;
        for (int i = 1; i <= fact; i++) {
            fact_res *= i;
        }
        System.out.println(fact_res);
    }
}
